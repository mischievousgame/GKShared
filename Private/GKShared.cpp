// Copyright 2023 Mischievous Game, Inc. All Rights Reserved.

#include "GKShared.h"

DEFINE_LOG_CATEGORY(LogGKShared)

// Unreal Engine
#include "Misc/Paths.h"
#include "Modules/ModuleManager.h"

#define LOCTEXT_NAMESPACE "FGKSharedModule"

void FGKSharedModule::StartupModule() 
{}

void FGKSharedModule::ShutdownModule()
{}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FGKSharedModule, GKShared)