// Copyright 2023 Mischievous Game, Inc. All Rights Reserved.

using System.IO;
using UnrealBuildTool;

public class GKShared : ModuleRules
{
    public GKShared(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.Add(Path.Combine(ModuleDirectory));

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "Engine",

            // GK Navigation Interface
            "AIModule",         // TeamAgent Interface & PathFollowingComponent
            "NavigationSystem", // Navigation Queries
        });

        PrivateDependencyModuleNames.AddRange(new string[] {
            "CoreUObject",

            // Rendering stuff from UKismetRenderingLibrary ?
            "RHI",
            "RenderCore",
        });
    }
}
