// Copyright 2023 Mischievous Game, Inc. All Rights Reserved.

#pragma once

#include "Modules/ModuleManager.h"
#include "Stats/Stats.h"

//*
DECLARE_LOG_CATEGORY_EXTERN(LogGKShared, Log, All);

#define GKSH_FATAL(Format, ...)       UE_LOG(LogGKShared, Fatal, Format, ##__VA_ARGS__)
#define GKSH_ERROR(Format, ...)       UE_LOG(LogGKShared, Error, Format, ##__VA_ARGS__)
#define GKSH_WARNING(Format, ...)     UE_LOG(LogGKShared, Warning, Format, ##__VA_ARGS__)
#define GKSH_DISPLAY(Format, ...)     UE_LOG(LogGKShared, Display, Format, ##__VA_ARGS__)
#define GKSH_LOG(Format, ...)         UE_LOG(LogGKShared, Log, Format, ##__VA_ARGS__)
#define GKSH_VERBOSE(Format, ...)     UE_LOG(LogGKShared, Verbose, Format, ##__VA_ARGS__)
#define GKSH_VERYVERBOSE(Format, ...) UE_LOG(LogGKShared, VeryVerbose, Format, ##__VA_ARGS__)

DECLARE_STATS_GROUP(TEXT("GKShared"), STATGROUP_GKAShader, STATCAT_Advanced);
//*/

class FGKSharedModule: public IModuleInterface
{
    public:
    /** IModuleInterface implementation */
    virtual void StartupModule() override;
    virtual void ShutdownModule() override;
};
