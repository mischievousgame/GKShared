// Copyright Mischievous Game, Inc. All Rights Reserved.

#pragma once

// Gamekit


// Unreal Engine
#include "AITypes.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Navigation/PathFollowingComponent.h"

// Generated
#include "GKNavigationInterface.generated.h"


DECLARE_LOG_CATEGORY_EXTERN(LogGKNav, Log, All);

USTRUCT(BlueprintType)
struct FGKNavQueryResult
{
    GENERATED_USTRUCT_BODY()

    //! Path to our destination
    FNavPathSharedPtr NavPath;

    //! Movement request id if any was issued (requires `PathFollowingComponent` to have been found)
    FAIRequestID RequestID;

    //! Success code, `EPathFollowingRequestResult` here is reused
    //! and it is always populated as the default error code for the entire request
    TEnumAsByte<EPathFollowingRequestResult::Type> Code;
};

/**
 * Move some of the AIController logic interfacing with the navigation system to a blueprint library
 * so the logic can be reused without AIControllers
 */
UCLASS()
class GKSHARED_API UGKNavigationInterface: public UBlueprintFunctionLibrary
{
    GENERATED_BODY()
    public:

    UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldCtx"), Category = "Pathfinding")
    static FGKNavQueryResult SimplifiedMove(UObject                            *WorldCtx,
                                            AActor                             *Actor,
                                            FVector const                      &Dest,
                                            AActor                             *GoalActor,
                                            TSubclassOf<UNavigationQueryFilter> FilterClass,
                                            bool                                bUsePathFinding,
                                            bool                                bMoveToActor,
                                            bool                                bAllowPartialPaths,
                                            bool                                bProjectDestinationToNavigation,
                                            float                               AcceptanceRadius,
                                            bool                                bStopOnOverlap,
                                            bool                                bCanStrafe,
                                            bool                                bDebug);

    UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldCtx"), Category = "Pathfinding")
    static FGKNavQueryResult FindPath(UObject                            *WorldCtx,
                                      AActor                             *Actor,
                                      FVector const                      &Dest,
                                      AActor                             *GoalActor,
                                      TSubclassOf<UNavigationQueryFilter> FilterClass,
                                      bool                                bUsePathFinding,
                                      bool                                bMoveToActor,
                                      bool                                bAllowPartialPaths,
                                      bool                                bProjectDestinationToNavigation,
                                      float                               AcceptanceRadius,
                                      bool                                bStopOnOverlap,
                                      bool                                bCanStrafe,
                                      bool                                bDebug);

    UFUNCTION(BlueprintPure, Category = "Pathfinding")
    static TArray<FVector> GetPathPoints(FGKNavQueryResult const &Path)
    {
        TArray<FVector> Points;

        if (Path.Code == EPathFollowingRequestResult ::RequestSuccessful && Path.NavPath)
        {
            Points.Reserve(Path.NavPath->GetPathPoints().Num());

            for (FNavPathPoint &Point: Path.NavPath->GetPathPoints())
            {
                Points.Add(Point.Location);
            }
        }
        return Points;
    }
};
